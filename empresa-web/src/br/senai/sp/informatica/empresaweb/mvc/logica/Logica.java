package br.senai.sp.informatica.empresaweb.mvc.logica;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface Logica {

	/**
	 * Esse metodo retorna uma pag jsp
	 * para que o dispatcher fa�a o encaminhamento
	 * @param req - requisi��o do usu�rio
	 * @param res - resposta ao servidor
	 * @return - pag jsp para ser exibida
	 * @throws Exception - erros que podem ocorrer
	 */
	public String executa(HttpServletRequest req, HttpServletResponse res) throws Exception;

}

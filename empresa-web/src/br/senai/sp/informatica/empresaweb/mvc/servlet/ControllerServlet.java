package br.senai.sp.informatica.empresaweb.mvc.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.senai.sp.informatica.empresaweb.mvc.logica.Logica;

//http://localhost:8080/mvc
@WebServlet("/mvc")
public class ControllerServlet extends HttpServlet {

	@Override
	protected void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		// obt�m o par�metro l�gica da requisi��o
		// http://localhost:8080/mvc?logica=xpto

		String parametro = req.getParameter("logica");

		// comp�e o nome da classe de l�gica
		// utilizando o para�metrinhu
		// br.senai.sp.informatica.agenda.mvc.logica.
		String className = "br.senai.sp.informatica.empresaweb.mvc.logica." + parametro;

		try {
			// carrega a classe para a mem�ria]//br.senai.sp.informatica.agenda.mvc.logica
			Class classe = Class.forName(className);

			// cria uma nova instancia da classe
			// de logica que esta na memoria
			Logica logica = (Logica) classe.newInstance();

			// executa o metodo da classe de logica
			// que foi passada
			// xpto.executa(req, res)
			// o m�todo executa deve retornado um
			// jsp, e o dispatcher deve encaminhar
			// o usu�rio para esse jsp

			String pagina = logica.executa(req, res);

			// cont�m o request dispatcher passando
			// para ele a pagina retornada do metodo
			// executa e encaminha para o usuario
			req.getRequestDispatcher(pagina).forward(req, res);

		} catch (Exception e) {
			throw new ServletException("A l�gica de neg�cios causou uma exce��o: ", e);
		}
	}

}

package br.senai.sp.informatica.empresaweb.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import br.senai.sp.informatica.empresaweb.model.Funcionario;

public class FuncionarioDao {

	// atributos
	Connection connection;

	// construtor
	public FuncionarioDao() {
		// abrindo conex�o com o b.d.d
		this.connection = new ConnectionFactory().getConnection();
	}

	// m�todo salva
	public void salva(Funcionario funcionario) {

		// comando sql
		String sql = "INSERT INTO funcionario" + "(nome, email, cpf, senha)" + "VALUES(?,?,?,?)";

		try {
			// cria um PreparedStatement com o comando sql
			// java.sql.PreparedStatement
			PreparedStatement stmt = connection.prepareStatement(sql);

			// cria os par�metros para as "?"s
			stmt.setString(1, funcionario.getNome());
			stmt.setString(2, funcionario.getEmail());
			stmt.setString(3, funcionario.getCpf());
			stmt.setString(4, funcionario.getSenha());

			// executa a instru��o sql
			stmt.execute();

			// libera o recurso de prepared statement
			stmt.close();

		} catch (SQLException e) {
			throw new RuntimeException(e);
		}finally {
			try {
				connection.close();
			} catch (SQLException e) {
				throw new RuntimeException(e);
			}
		}
	}// fim do m�todo salva

	// m�todo getLista

	public List<Funcionario> getLista() {
		try {
			// cria uma lista de contatos

			List<Funcionario> funcionarios = new ArrayList<>();

			PreparedStatement stmt = connection.prepareStatement("SELECT * FROM funcionario");

			// guarda os resultados da consulta em um ResultSet(conjunto de resultados)
			ResultSet rs = stmt.executeQuery();

			// enquanto houver um pr�ximo registro no ResultSet...
			while (rs.next()) {

				// cria um novo contato com os dados do ResultSet
				Funcionario funcionario = new Funcionario();
				funcionario.setId(rs.getLong("id"));
				funcionario.setNome(rs.getString("nome"));
				funcionario.setEmail(rs.getString("email"));
				funcionario.setCpf(rs.getString("cpf"));
				funcionario.setSenha(rs.getString("senha"));

				// adiciona o contato ao ArrayList contatos
				funcionarios.add(funcionario);
			} // fim do while

			// libera o recurso de ResultSet
			rs.close();

			// libera o preparedStatement
			stmt.close();

			// retorna a lista de contatos
			return funcionarios;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				throw new RuntimeException(e);
			}
		}
	}// fim do m�todo getLista

	// m�todo excluir
	public void excluir(Funcionario funcionario) {
		try {
			PreparedStatement stmt = connection.prepareStatement("DELETE FROM funcionario WHERE id = ?");
			stmt.setLong(1, funcionario.getId());
			stmt.execute();
			stmt.close();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				throw new RuntimeException(e);
			}
		}
	}
} //fim da classe
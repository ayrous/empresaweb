package br.senai.sp.informatica.empresaweb.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import br.senai.sp.informatica.empresaweb.dao.FuncionarioDao;
import br.senai.sp.informatica.empresaweb.model.Funcionario;

@WebServlet("/adicionaFuncionario")
public class AdicionaFuncionarioServlet extends HttpServlet {

	@Override
	protected void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {

		// obt�m um PrintWriter
		// esse objeto nos permitir� escrever mensagens no response
		PrintWriter out = res.getWriter();

		// pega os par�metros da requisi��o
		// por meio dos names do form

		String nome = req.getParameter("nome");
		String email = req.getParameter("email");
		String cpf = req.getParameter("cpf");
		String senha = req.getParameter("senha");

		// cria uma nova instancia de contato
		Funcionario funcionario = new Funcionario();

		// define os atributos do contato
		funcionario.setNome(nome);
		funcionario.setEmail(email);
		funcionario.setCpf(cpf);
		funcionario.setSenha(senha);

		// obtem uma instancia de dao
		// abre uma conex�o com o banco de dados
		FuncionarioDao dao = new FuncionarioDao();

		// salva o contato no banco de dados
		dao.salva(funcionario);

		// feedback para o usuario
		
		//cria um request dispatcher
		RequestDispatcher dispatcher = req.getRequestDispatcher("/funcionario-adicionado.jsp");
		
		//encaminha o usuario para essa pag
		dispatcher.forward(req, res);
	}// fim do m�todo service

}// fim da classe
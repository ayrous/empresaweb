<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%--cabeçalho da taglib core --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%-- tag de formatação --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Lista de Funcionários</title>
<link href="estilo.css" rel="stylesheet"/>

</head>
<body>

	<c:import url="cabecalho.jsp"></c:import>


	<h2>Tabela dos Funcionarios</h2>
	<table border="2" style="border-collapse: collape;">

		<%-- cabeçalho --%>
		<tr style="font-weight: bold;">

			<th>Nº</th>
			<th>Nome</th>
			<th>Email</th>
			<th>Cpf</th>
			<th colspan="2">Opções</th>
		</tr>

		<c:forEach var="funcionario" items="${funcionarios}" varStatus="id">
			<tr bgcolor="#${id.count % 2 == 0 ? 'AAEE88' : 'ac00e6' }">
				<td>${id.count}</td>
				<td>${funcionario.nome}</td>
				<td><c:if test="${not empty funcionario.email }">
						<a href="mailto:${funcionario.email }" style="color: blue;">
							${funcionario.email } </a>
					</c:if> <c:if test="${empty funcionario.email }">
					Email não informado!
					</c:if></td>
				<td>${funcionario.cpf}</td>
				
				<td>
					<a href="mvc?logica=RemoveFuncionarioLogica&id=${funcionario.id }">Excluir</a>
				</td>
			</tr>
		</c:forEach>
	</table>
	
	<a href="http://localhost:8080/empresa-web/">
	Voltar para o Home</a>
	


	<c:import url="rodape.jsp"></c:import>
</body>
</html>
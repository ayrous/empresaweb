<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="estilo.css" rel="stylesheet" />
<title>YEAAAH!</title>
</head>
<body>

	<c:import url="cabecalho.jsp"></c:import>

	Funcionario ${param.nome} adicionado com sucesso!
	<img alt="imagem de pessoas comemorando" src="imgs/yeah.png">
	<br>
	<br>
	<a href="http://localhost:8080/empresa-web/"> Voltar para o Home</a>

	<c:import url="rodape.jsp"></c:import>

</body>
</html>